export const blue = {

  '--primary' : '#FFFFFF',
  '--primary-variant': '#000000',

  '--secondary': '#ffeb3b',
  '--secondary-variant': '#ffff72',

  '--background': '#8a7251',
  '--surface': '#2d3728',
  '--dialog': '#000000',
  '--cancel': '#9E9E9E',

  '--on-primary': '#000000',
  '--on-secondary': '#f44336',
  '--on-background': '#f44336',
  '--on-surface': '#ffffff',
  '--on-cancel': '#000000',

  '--green': '#4caf50',
  '--red': '#f44336',
  '--yellow': '#FFD54F',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Forest-Theme',
      'de': 'Wald-Theme'
    },
    'description': {
      'en': 'Forest-Colors',
      'de': 'Wald-Farben'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
